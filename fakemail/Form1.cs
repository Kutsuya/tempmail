﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using CefSharp;
using CefSharp.WinForms;
using System.IO;
using System.Runtime.CompilerServices;
using System.Timers;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace fakemail
{
    public partial class Form1 : Form
    {
        System.Timers.Timer aTimer = new System.Timers.Timer();
        public bool LookingForActivation = false;
        public bool ActivationFound = false;
        public string MailLink;

        public Form1()
        {
            InitializeComponent();
            InitializeChromium();
            chromeBrowser.FrameLoadEnd += WebBrowserFrameLoadEnded;
        }

        public ChromiumWebBrowser chromeBrowser;

        public void InitializeChromium()
        {
            CefSettings settings = new CefSettings();
            // Initialize cef with the provided settings
            Cef.Initialize(settings);
            settings.CefCommandLineArgs.Add("--disable-local-storage", "1");
            // Create a browser component
            chromeBrowser = new ChromiumWebBrowser("https://generator.email");
            // Add it to the form and fill it to the form window.
            Controls.Add(chromeBrowser);
            chromeBrowser.Width = 1;
            chromeBrowser.Height = 1;
        }

        private void WebBrowserFrameLoadEnded(object sender, FrameLoadEndEventArgs e)
        {
            if (e.Frame.IsMain)
            {
                chromeBrowser.GetSourceAsync().ContinueWith(taskHtml =>
                {
                    try
                    {
                        if (LookingForActivation)
                        {
                            return;
                        }
                        Console.WriteLine("Loaded");
                        string html = taskHtml.Result;

                        HtmlDocument doc = new HtmlDocument();
                        doc.Load(new StringReader(html));
                        
                        // Find the current email
                        string username = doc.GetElementbyId("userName").GetAttributeValue("value", "");
                        string domainname = doc.GetElementbyId("domainName2").GetAttributeValue("value", "");

                        // Write the current email to the textbox
                        MailLink = e.Url + "//" + username + "@" + domainname;
                        txtEmail.Invoke(new Action(() => txtEmail.Text = username + "@" + domainname));
                        txtEmail.Invoke(new Action(() => txtEmail.Enabled = true));
                        btnCopyEmail.Invoke(new Action(() => btnCopyEmail.Enabled = true));
                        lblInfo.Invoke(new Action(() => lblInfo.Visible = true));

                        username = username.Replace(".", string.Empty); // or index + 1 to keep slash

                        int index_domainname = domainname.LastIndexOf(".");
                        if (index_domainname > 0) domainname = domainname.Substring(0, index_domainname); // or index + 1 to keep slash

                        txtUsername.Invoke(new Action(() => txtUsername.Text = username + domainname));

                        if (!LookingForActivation)
                        {
                            // Begin refresh timer
                            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                            // Set the Interval to 3 seconds.
                            aTimer.Interval = 3000;
                            aTimer.Enabled = true;
                            LookingForActivation = true;
                        }

                        if (ActivationFound)
                        {
                            MailLink = e.Url + "//" + username + "@" + domainname;
                            html = taskHtml.Result;
                            doc = new HtmlDocument();
                            doc.Load(new StringReader(html));

                            var linksOnPage = from lnks in doc.DocumentNode.Descendants()
                                where lnks.Name == "a" &&
                                      lnks.Attributes["href"] != null &&
                                      lnks.InnerText.Trim().Length > 0
                                select new
                                {
                                    Url = lnks.Attributes["href"].Value,
                                    Text = lnks.InnerText
                                };
                            foreach (var link in linksOnPage)
                            {
                                if (!link.Url.Contains("signup.php")) continue;
                                txtUrl.Invoke(new Action(() => txtUrl.Text = link.Url));
                                chromeBrowser.Load(link.Url);

                            }
                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally
                    {
                        linkLabelOpenMail.Invoke(new Action(() => linkLabelOpenMail.Enabled = true));
                        Blink();
                    }
                });
            }
        }
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Refreshing page...");
            FindUrl("signup.php");

            if (LookingForActivation)
            {
                FindUrl("signup.php");
            }
            else
            {
                FindUrl("Account Activation");
            }
        }



        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Cef.Shutdown();
        }

        private void linkLabelGenerate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            chromeBrowser.Load("https://generator.email/email-generator");
            MailLink = "";
            linkLabelOpenMail.Enabled = false;
        }

        private void linkLabelRefresh_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            chromeBrowser.Load("https://generator.email/" + txtEmail.Text);
        }

        private void btnCopyEmail_Click(object sender, EventArgs e)
        {
            if (txtEmail.Text != String.Empty)
            {
                Clipboard.SetText(txtEmail.Text);
                MessageBox.Show("Copied " + txtEmail.Text, "Copied!");
            }
        }

        private void btnCopyUrl_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtUrl.Text);
            MessageBox.Show("Copied " + txtUrl.Text, "Copied!");
        }

        private void FindUrl(string type)
        {
            chromeBrowser.GetSourceAsync().ContinueWith(taskHtml =>
            {
                try
                {
                    var html = taskHtml.Result;

                    var doc = new HtmlDocument();
                    doc.Load(new StringReader(html));

                    var linksOnPage = from lnks in doc.DocumentNode.Descendants()
                        where lnks.Name == "a" &&
                              lnks.Attributes["href"] != null &&
                              lnks.InnerText.Trim().Length > 0
                        select new
                        {
                            Url = lnks.Attributes["href"].Value,
                            Text = lnks.InnerText
                        };

                    foreach (var link in linksOnPage) 
                    {
                        if (link.Text.Contains(type))
                        {
                            Console.WriteLine(link.Text);
                            Console.WriteLine(link.Url);
                            if (LookingForActivation == true)
                            {
                                ActivationFound = true;
                                txtUrl.Invoke(new Action(() => txtUrl.Text = link.Url));
                                txtUrl.Invoke(new Action(() => txtUrl.Enabled = true));
                                btnCopyUrl.Invoke(new Action(() => btnCopyUrl.Enabled = true));
                                aTimer.Stop();
                            }
                            if (type.Equals("signup.php") && LookingForActivation == false)
                            {
                                LookingForActivation = true;
                            }
                            chromeBrowser.Load("https://generator.email/" + link.Url);

                        }
                    }

                    string username = doc.GetElementbyId("userName").GetAttributeValue("value", "");
                    string domainname = doc.GetElementbyId("domainName2").GetAttributeValue("value", "");


                    txtEmail.Invoke(new Action(() => txtEmail.Text = username + "@" + domainname));

                    username = username.Replace(".", string.Empty); // or index + 1 to keep slash

                    int index_domainname = domainname.LastIndexOf(".");
                    if (index_domainname > 0) domainname = domainname.Substring(0, index_domainname); // or index + 1 to keep slash

                    txtUsername.Invoke(new Action(() => txtUsername.Text = username + domainname));
                }
                catch (Exception ex)
                {

                }
            });
        }

        private void linkLabelFindUrl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void linkLabelOpenMail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://generator.email/" + txtEmail.Text);
        }

        private async void Blink()
        {
            while (true)
            {
                await Task.Delay(500);
                lblWarning.ForeColor = lblWarning.ForeColor == Color.Red ? Color.Black : Color.Red;
            }
        }
    }
}
