﻿namespace fakemail
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCopyUrl = new System.Windows.Forms.Button();
            this.btnCopyEmail = new System.Windows.Forms.Button();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtConnections = new System.Windows.Forms.TextBox();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.lblInfo = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.linkLabelFindUrl = new System.Windows.Forms.LinkLabel();
            this.linkLabelRefresh = new System.Windows.Forms.LinkLabel();
            this.linkLabelGenerate = new System.Windows.Forms.LinkLabel();
            this.linkLabelOpenMail = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblWarning = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblWarning);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.btnCopyUrl);
            this.panel1.Controls.Add(this.btnCopyEmail);
            this.panel1.Controls.Add(this.lblEmail);
            this.panel1.Controls.Add(this.txtConnections);
            this.panel1.Controls.Add(this.txtServer);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.txtUsername);
            this.panel1.Controls.Add(this.txtUrl);
            this.panel1.Controls.Add(this.lblInfo);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtEmail);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(141, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(389, 397);
            this.panel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(84, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(293, 30);
            this.label5.TabIndex = 3;
            this.label5.Text = "These only work after activating the account\r\nGo to hitnews.com and signup.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnCopyUrl
            // 
            this.btnCopyUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyUrl.Enabled = false;
            this.btnCopyUrl.Location = new System.Drawing.Point(302, 32);
            this.btnCopyUrl.Name = "btnCopyUrl";
            this.btnCopyUrl.Size = new System.Drawing.Size(75, 20);
            this.btnCopyUrl.TabIndex = 1;
            this.btnCopyUrl.Text = "Copy URL";
            this.btnCopyUrl.UseVisualStyleBackColor = true;
            this.btnCopyUrl.Click += new System.EventHandler(this.btnCopyUrl_Click);
            // 
            // btnCopyEmail
            // 
            this.btnCopyEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyEmail.Enabled = false;
            this.btnCopyEmail.Location = new System.Drawing.Point(302, 6);
            this.btnCopyEmail.Name = "btnCopyEmail";
            this.btnCopyEmail.Size = new System.Drawing.Size(75, 20);
            this.btnCopyEmail.TabIndex = 1;
            this.btnCopyEmail.Text = "Copy Email";
            this.btnCopyEmail.UseVisualStyleBackColor = true;
            this.btnCopyEmail.Click += new System.EventHandler(this.btnCopyEmail_Click);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(6, 9);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(35, 13);
            this.lblEmail.TabIndex = 0;
            this.lblEmail.Text = "Email:";
            // 
            // txtConnections
            // 
            this.txtConnections.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConnections.Location = new System.Drawing.Point(84, 136);
            this.txtConnections.Name = "txtConnections";
            this.txtConnections.Size = new System.Drawing.Size(293, 20);
            this.txtConnections.TabIndex = 2;
            this.txtConnections.Text = "30";
            // 
            // txtServer
            // 
            this.txtServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServer.Location = new System.Drawing.Point(84, 110);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(293, 20);
            this.txtServer.TabIndex = 2;
            this.txtServer.Text = "free.hitnews.com";
            // 
            // txtUsername
            // 
            this.txtUsername.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUsername.Location = new System.Drawing.Point(84, 58);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(293, 20);
            this.txtUsername.TabIndex = 2;
            // 
            // txtUrl
            // 
            this.txtUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUrl.Enabled = false;
            this.txtUrl.Location = new System.Drawing.Point(84, 32);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(212, 20);
            this.txtUrl.TabIndex = 2;
            // 
            // lblInfo
            // 
            this.lblInfo.Location = new System.Drawing.Point(9, 356);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(377, 19);
            this.lblInfo.TabIndex = 0;
            this.lblInfo.Text = "The application will keep checking for an activation email every 3 seconds.";
            this.lblInfo.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Connections:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Server:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Username:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "URL:";
            // 
            // txtEmail
            // 
            this.txtEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmail.Enabled = false;
            this.txtEmail.Location = new System.Drawing.Point(84, 6);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(212, 20);
            this.txtEmail.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.Controls.Add(this.linkLabelOpenMail);
            this.panel3.Controls.Add(this.linkLabelFindUrl);
            this.panel3.Controls.Add(this.linkLabelRefresh);
            this.panel3.Controls.Add(this.linkLabelGenerate);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(141, 397);
            this.panel3.TabIndex = 2;
            // 
            // linkLabelFindUrl
            // 
            this.linkLabelFindUrl.AutoSize = true;
            this.linkLabelFindUrl.Location = new System.Drawing.Point(12, 65);
            this.linkLabelFindUrl.Name = "linkLabelFindUrl";
            this.linkLabelFindUrl.Size = new System.Drawing.Size(52, 13);
            this.linkLabelFindUrl.TabIndex = 0;
            this.linkLabelFindUrl.TabStop = true;
            this.linkLabelFindUrl.Text = "Find URL";
            this.linkLabelFindUrl.Visible = false;
            this.linkLabelFindUrl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelFindUrl_LinkClicked);
            // 
            // linkLabelRefresh
            // 
            this.linkLabelRefresh.AutoSize = true;
            this.linkLabelRefresh.LinkColor = System.Drawing.Color.Black;
            this.linkLabelRefresh.Location = new System.Drawing.Point(12, 28);
            this.linkLabelRefresh.Name = "linkLabelRefresh";
            this.linkLabelRefresh.Size = new System.Drawing.Size(122, 13);
            this.linkLabelRefresh.TabIndex = 0;
            this.linkLabelRefresh.TabStop = true;
            this.linkLabelRefresh.Text = "Refresh current account";
            this.linkLabelRefresh.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelRefresh_LinkClicked);
            // 
            // linkLabelGenerate
            // 
            this.linkLabelGenerate.AutoSize = true;
            this.linkLabelGenerate.LinkColor = System.Drawing.Color.Black;
            this.linkLabelGenerate.Location = new System.Drawing.Point(12, 9);
            this.linkLabelGenerate.Name = "linkLabelGenerate";
            this.linkLabelGenerate.Size = new System.Drawing.Size(116, 13);
            this.linkLabelGenerate.TabIndex = 0;
            this.linkLabelGenerate.TabStop = true;
            this.linkLabelGenerate.Text = "Generate new account";
            this.linkLabelGenerate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelGenerate_LinkClicked);
            // 
            // linkLabelOpenMail
            // 
            this.linkLabelOpenMail.AutoSize = true;
            this.linkLabelOpenMail.Enabled = false;
            this.linkLabelOpenMail.LinkColor = System.Drawing.Color.Black;
            this.linkLabelOpenMail.Location = new System.Drawing.Point(12, 46);
            this.linkLabelOpenMail.Name = "linkLabelOpenMail";
            this.linkLabelOpenMail.Size = new System.Drawing.Size(55, 13);
            this.linkLabelOpenMail.TabIndex = 1;
            this.linkLabelOpenMail.TabStop = true;
            this.linkLabelOpenMail.Text = "Open Mail";
            this.linkLabelOpenMail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelOpenMail_LinkClicked);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Password:";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(84, 84);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(293, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.Text = "Welkom#1";
            // 
            // lblWarning
            // 
            this.lblWarning.Location = new System.Drawing.Point(84, 189);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(293, 15);
            this.lblWarning.TabIndex = 3;
            this.lblWarning.Text = "TOR Browser is required!";
            this.lblWarning.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 397);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.MinimumSize = new System.Drawing.Size(546, 436);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fakemail";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.LinkLabel linkLabelRefresh;
        private System.Windows.Forms.LinkLabel linkLabelGenerate;
        private System.Windows.Forms.Button btnCopyEmail;
        private System.Windows.Forms.Button btnCopyUrl;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabelFindUrl;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.TextBox txtConnections;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel linkLabelOpenMail;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblWarning;
    }
}

